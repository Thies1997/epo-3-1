library IEEE;
use IEEE.std_logic_1164.ALL;

architecture structural of gotygolddeluxe is

-- Circle shit
signal value1, value2, result :std_logic_vector(7 downto 0) := "00000000";
signal done, start, started :std_logic;
signal x_circle, x_block_0, x_block_1 :std_logic_vector(6 downto 0) := "0000000";
signal y_circle, y_block :std_logic_vector(7 downto 0) := "00000000";
signal x_red, x_blue, x00, x01, x10, x11 : std_logic_vector(6 downto 0) := "0000000";
signal y_red, y_blue :std_logic_vector(7 downto 0);
signal height : std_logic_vector(5 downto 0);
--signal rip : std_logic;

signal quadrant : std_logic_vector(1 downto 0);
signal step : std_logic_vector(3 downto 0);


--signal cnt: std_logic;
signal btype, hit_block_type: std_logic_vector (2 downto 0);

--signal output_y0, output_y1, output_y2, output_y3 :unsigned(7 downto 0);
signal block_y0, block_y1, block_y2, block_y3 :std_logic_vector(7 downto 0);
signal output_type0, output_type1, output_type2, output_type3 :std_logic_vector(2 downto 0);


component hitman2 is
	port(clk    :in    std_logic;
		reset  :in    std_logic;
		x_red  :in    std_logic_vector(6 downto 0);
		x_blue :in    std_logic_vector(6 downto 0);
		y_red  :in    std_logic_vector(7 downto 0);
		y_blue :in    std_logic_vector(7 downto 0);
		type0  :in    std_logic_vector(2 downto 0);
		type1  :in    std_logic_vector(2 downto 0);
		height :in    std_logic_vector(5 downto 0);
		y0     :in    std_logic_vector(7 downto 0);
		y1     :in    std_logic_vector(7 downto 0);
		done   :in    std_logic;
		started:in    std_logic;
		start  :out   std_logic;
		block_type :out std_logic_vector(2 downto 0);
		x_circle :out std_logic_vector(6 downto 0) := "0000000";
		y_circle, y_block              :out std_logic_vector(7 downto 0) := "00000000"
);
end component;

component hit_math_unit is
	port(clk      :in	std_logic;
		reset    :in	std_logic;
		start    :in	std_logic;
		x_circle :in	std_logic_vector(6 downto 0);
		y_circle :in	std_logic_vector(7 downto 0);
		x_block_0:in	std_logic_vector(6 downto 0);
		x_block_1:in	std_logic_vector(6 downto 0);
		y_block  :in	std_logic_vector(7 downto 0);
		height   :in	std_logic_vector(5 downto 0);
		value1   :out	std_logic_vector(7 downto 0);
		value2   :out	std_logic_vector(7 downto 0);
		result   :in	std_logic_vector(7 downto 0);
		started  :out	std_logic;
		done     :out	std_logic;
		rip      :out	std_logic);
end component;

component subtraction is
	port(clk   :in    std_logic;
		reset  :in    std_logic;
		value1 :in    std_logic_vector(7 downto 0);
		value2 :in    std_logic_vector(7 downto 0);
		result :out   std_logic_vector(7 downto 0));
end component;

component decoder is
	port(clk			:in std_logic;
		reset      :in std_logic;
		block_type :in    std_logic_vector(2 downto 0);
		x0         :out   std_logic_vector(6 downto 0);
		x1         :out   std_logic_vector(6 downto 0);
		height     :out   std_logic_vector(5 downto 0));
end component;

component controller is
	port(clk  :in    std_logic;
		counter_clk		:in    std_logic;
		reset	  :in    std_logic;
		left    :in    std_logic;
		right   :in    std_logic;
		quadrant:out   std_logic_vector(1 downto 0);
		step    :out   std_logic_vector(3 downto 0)
);
end component;

component coordcalc is
	port(clk		:in    std_logic;
		reset	:in    std_logic;
		quadrant:in    std_logic_vector(1 downto 0);
		step    :in    std_logic_vector(3 downto 0);
		x_red   :out   std_logic_vector(6 downto 0);
		y_red   :out   std_logic_vector(7 downto 0);
		x_blue  :out   std_logic_vector(6 downto 0);
		y_blue  :out   std_logic_vector(7 downto 0)
);
end component;

component typebuilder
	port (
		clk         :in  std_logic;                                     
		reset       :in  std_logic;
		blocktype   :out std_logic_vector (2 downto 0)
	);
end component;

component counter
	port (clk       : in std_logic;
		reset     : in std_logic;
		rip					: in std_logic;
		count_out : out std_logic
	);
end component;

component bar
	port( counter      :in     std_logic;
		reset        :in     std_logic;
		blocktype    :in     std_logic_vector(2 downto 0);
		output_y0    :out    std_logic_vector(7 downto 0);
		output_y1    :out    std_logic_vector(7 downto 0);
		output_y2    :out    std_logic_vector(7 downto 0);
		output_y3    :out    std_logic_vector(7 downto 0);
		output_type0 :out    std_logic_vector(2 downto 0);
		output_type1 :out    std_logic_vector(2 downto 0);
		output_type2 :out    std_logic_vector(2 downto 0);
		output_type3 :out    std_logic_vector(2 downto 0)
	);
end component;

component vga_module is
 	port( 	clk, reset: in std_logic;
		x_b1: in std_logic_vector(6 downto 0); -- x van 0 tot 119 met 20 meest rechtse binnen scherm en 99 meeste linkse binne scherm
		y_b1: in std_logic_vector(7 downto 0); -- y van 0 tot 159 met 0 boven en 159 onder
		x_b2: in std_logic_vector(6 downto 0);
		y_b2: in std_logic_vector(7 downto 0);
		y_o1: in std_logic_vector(7 downto 0);
		t_o1: in std_logic_vector(2 downto 0); -- 010 is links balk 001 is rechts balk rest nog niets
		y_o2: in std_logic_vector(7 downto 0);
		t_o2: in std_logic_vector(2 downto 0);
		y_o3: in std_logic_vector(7 downto 0);
		t_o3: in std_logic_vector(2 downto 0);
		y_o4: in std_logic_vector(7 downto 0);
		t_o4: in std_logic_vector(2 downto 0);
		red,green,blue: out std_logic;
		hsync: out std_logic;
		vsync: out std_logic);
end component;

begin

-- block_y0 <= std_logic_vector(output_y0);
-- block_y1 <= std_logic_vector(output_y1);
-- block_y2 <= std_logic_vector(output_y2);
-- block_y3 <= std_logic_vector(output_y3);

hit_states: hitman2 port map(
		clk => clk,			-- in
		reset => reset,		-- in
		-- Circle coordinates
		x_red => x_red,		-- in
		x_blue => x_blue,		-- in
		y_red => y_red,		-- in
		y_blue => y_blue,	-- in
		-- Block coordinates
		type0 => output_type0,	-- in
		type1 => output_type1,	-- in
		height => height,		-- in
		y0 => block_y0,		-- in
		y1 => block_y1,		-- in
		-- Flags
		done => done,		-- in
		started => started,	-- in
		start => start,		-- out
		-- Coordinates to be used by the math unit
		x_circle => x_circle,	-- out
		block_type => hit_block_type,	-- out
		y_circle => y_circle,	-- out
		y_block => y_block	-- out
);

math_unit: hit_math_unit port map (
		clk => clk,			-- in
		reset => reset,		-- in
		start => start,		-- in
		-- Coordinate of 'a' circle, either red or blue
		x_circle => x_circle,	-- in
		y_circle => y_circle,	-- in
		-- X-coordinates of start and end of one of the two blocks
		x_block_0 => x_block_0,	-- in
		x_block_1 => x_block_1,	-- in
		-- Y-coordinate of the block and it's height
		y_block => y_block,	-- in
		height => height,		-- in
		-- Values to route to the subtractor, to compare values
		value1 => value1,	-- out
		value2 => value2,	-- out
		result => result,		-- in
		-- Flags
		started => started,	-- out
		done => done,		-- out
		rip => rip			-- out
);

-- Result = value1 - value2;
subtractor: subtraction port map (
	clk => clk,			-- in
	reset => reset,		-- in
	value1 => value1,	-- in
	value2 => value2,	-- in
	result => result		-- out
);

type_decoder: decoder port map(
		clk,				-- in
		reset,			-- in
		hit_block_type,	-- in
		x_block_0,		-- out
		x_block_1,		-- out
		height			-- out
);

input_controller: controller port map (
		clk => clk,
		counter_clk => cnt,
		reset => reset,
		left => left,
		right => right,
		quadrant => quadrant,
		step => step
);

coordinate_generator: coordcalc port map (
		clk => clk,
		reset => reset,
		quadrant => quadrant,
		step => step,
		x_red => x_red,
		y_red => y_red,
		x_blue => x_blue,
		y_blue => y_blue
);

type_generator: typebuilder port map (
		cnt,		-- in
		reset,	-- in
		btype	-- out
);
            
clock60: counter port map (
		clk => clk,			-- in
		reset => reset,		-- in
		rip => rip,			-- in
		count_out => cnt 	-- out
);
            
bar_movement: bar port map (
		counter => cnt,				-- in
		reset => reset,				-- in
		blocktype => btype,			-- in
		output_y0 => block_y0,		-- out
		output_y1 => block_y1,		-- out
		output_y2 => block_y2,		-- out
		output_y3 => block_y3,		-- out
		output_type0 => output_type0,	-- out
		output_type1 => output_type1,	-- out
		output_type2 => output_type2,	-- out
		output_type3 => output_type3	-- out
);

video_out: vga_module port map (
		clk => clk,
		reset => reset,
		x_b1 => x_red,			-- in
		y_b1 => y_red,			-- in
		x_b2 => x_blue,			-- in
		y_b2 => y_blue,			-- in
		y_o1 => block_y0,		-- in
		t_o1 => output_type0,	-- in
		y_o2 => block_y1,		-- in
		t_o2 => output_type1,	-- in
		y_o3 => block_y2,		-- in
		t_o3 => output_type2,	-- in
		y_o4 => block_y3,		-- in
		t_o4 => output_type3,	-- in
		red => red,
		green => green,
		blue => blue,
		hsync => hsync,
		vsync => vsync
);

end structural;
