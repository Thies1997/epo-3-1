library ieee;
use ieee.std_logic_1164.all;

architecture structural of vga_module is
component vga_core is
	PORT(clk, reset 	:in 	std_logic;
		x_b1     	:in	std_logic_vector(6 downto 0);
		y_b1     	:in	std_logic_vector(7 downto 0);
		x_b2     	:in	std_logic_vector(6 downto 0);
		y_b2     	:in	std_logic_vector(7 downto 0);
		y_o1     	:in	std_logic_vector(7 downto 0);
		t_o1     	:in	std_logic_vector(2 downto 0);
		y_o2     	:in	std_logic_vector(7 downto 0);
		t_o2     	:in	std_logic_vector(2 downto 0);
		y_o3     	:in	std_logic_vector(7 downto 0);
		t_o3     	:in	std_logic_vector(2 downto 0);
		y_o4     	:in	std_logic_vector(7 downto 0);
		t_o4     	:in	std_logic_vector(2 downto 0);
		y2_o1     :in    std_logic_vector(7 downto 0);
		y2_o2     :in    std_logic_vector(7 downto 0);
		y2_o3     :in    std_logic_vector(7 downto 0);
		y2_o4     :in    std_logic_vector(7 downto 0);
		y2_b1     :in    std_logic_vector(7 downto 0);
		y2_b2     :in    std_logic_vector(7 downto 0);
		x2_b1     :in    std_logic_vector(6 downto 0);
		x2_b2     :in    std_logic_vector(6 downto 0);
		rom_data	 :in    std_logic_vector(7 downto 0);
		rom_address	  :out	 std_logic_vector(4 downto 0);
		row_address    :in 	std_logic_vector(6 downto 0);
		col_address 	  :in 	std_logic_vector(7 downto 0);
		color: out std_logic_vector(1 downto 0);    
		rom_mux_output : out std_logic
	);
end component;

component vga_startscherm is
	PORT(clk, reset  :in 	std_logic;
	rom_data	 :in    std_logic_vector(7 downto 0);
	rom_address	 :out	std_logic_vector(5 downto 0);
	row_address      :in 	std_logic_vector(6 downto 0);
    col_address 	 :in 	std_logic_vector(7 downto 0);   
    rom_mux_output   :out   std_logic);
end component;

component rom is
	port ( address : in std_logic_vector(6 downto 0);
		q : out std_logic_vector(7 downto 0) ); 
end component;

component rom2 is
  port ( address : in std_logic_vector(5 downto 0);
         q : out std_logic_vector(7 downto 0) ); 
end component;


component coor_calc is
   port(y_o1 :in    std_logic_vector(7 downto 0);
        y_o2 :in    std_logic_vector(7 downto 0);
        y_o3 :in    std_logic_vector(7 downto 0);
        y_o4 :in    std_logic_vector(7 downto 0);
        y_b1 :in    std_logic_vector(7 downto 0);
        y_b2 :in    std_logic_vector(7 downto 0);
        x_b1 :in    std_logic_vector(6 downto 0);
        x_b2 :in    std_logic_vector(6 downto 0);
        y2_o1:out   std_logic_vector(7 downto 0);
        y2_o2:out   std_logic_vector(7 downto 0);
        y2_o3:out   std_logic_vector(7 downto 0);
        y2_o4:out   std_logic_vector(7 downto 0);
        y2_b1:out   std_logic_vector(7 downto 0);
        y2_b2:out   std_logic_vector(7 downto 0);
        x2_b1:out   std_logic_vector(6 downto 0);
        x2_b2:out   std_logic_vector(6 downto 0));
end component;

component vga_output is
   port(enable,rom_mux_output,rom2_mux_output:in    std_logic;
	color	:in std_logic_vector(1 downto 0);
        vga_red       :out   std_logic;
        vga_green     :out   std_logic;
        vga_blue      :out   std_logic);
end component;

component vga_sync is
 	port(	clk, reset : in std_logic;
 		hsync, vsync : out std_logic;
 		row_address : out std_logic_vector(6 downto 0);
 		col_address : out std_logic_vector(7 downto 0));
end component;
		
signal 	row: std_logic_vector(8 downto 0);
signal 	column : std_logic_vector(7 downto 0); 
signal row_address: std_logic_vector(6 downto 0);
signal col_address: std_logic_vector(7 downto 0);
signal rom_mux,rom2_mux: std_logic;
signal rom_address: std_logic_vector(4 downto 0);
signal rom2_address: std_logic_vector(5 downto 0);
signal rom_data,rom2_data:	std_logic_vector(7 downto 0);
signal y2_o1     :    std_logic_vector(7 downto 0);
signal y2_o2     :   std_logic_vector(7 downto 0);
signal y2_o3     :    std_logic_vector(7 downto 0);
signal y2_o4     :   std_logic_vector(7 downto 0);
signal y2_b1     :   std_logic_vector(7 downto 0);
signal y2_b2     :   std_logic_vector(7 downto 0);
signal x2_b1     :   std_logic_vector(6 downto 0);
signal x2_b2     :   std_logic_vector(6 downto 0);
signal color: std_logic_vector(1 downto 0);
signal score0,score1,score2 : std_logic_vector(3 downto 0);

begin

a1 : vga_sync port map (clk, reset, hsync, vsync, row_address, col_address);

a2: coor_calc port map(y_o1,y_o2,y_o3,y_o4,y_b1,y_b2,x_b1,x_b2,y2_o1,y2_o2,y2_o3,y2_o4,y2_b1,y2_b2,x2_b1,x2_b2);
a3: rom port map(rom_address,rom_data);
a4: rom2 port map(rom2_address,rom2_data);
a5: vga_core port map(clk, reset,
	x_b1,
	y_b1,     	
	x_b2,     	
	y_b2,    	
	y_o1,   	
	t_o1,     	
	y_o2,     	
	t_o2,     	
	y_o3,     	
	t_o3,     	
	y_o4,     	
	t_o4,     	
	y2_o1,     
	y2_o2,  
	y2_o3,    
	y2_o4,     
	y2_b1,     
	y2_b2,     
	x2_b1,     
	x2_b2,     
	rom_data,	 
	rom_address,	  
	row_address,    
	col_address,
	color, 	         
	rom_mux, score0,score1,score2
);
a6: vga_output port map(enable,rom_mux,rom2_mux,color,red,green,blue);
a7: vga_startscherm port map(clk, reset,     
	rom2_data,	 
	rom2_address,	  
	row_address,    
	col_address, 	         
	rom2_mux
);

end architecture;
