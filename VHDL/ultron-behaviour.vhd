library IEEE;
use IEEE.std_logic_1164.ALL;


architecture behaviour of ultron is

component controller is
   port(clk		:in    std_logic;
	reset	:in    std_logic;
	left    :in    std_logic;
        right   :in    std_logic;
        quadrant:out   std_logic_vector(1 downto 0);
        step    :out   std_logic_vector(3 downto 0));
end component;

component coordcalc is
   port(clk		:in    std_logic;
	reset	:in    std_logic;
	quadrant:in    std_logic_vector(1 downto 0);
        step    :in    std_logic_vector(3 downto 0);
	sinarray:out   std_logic_vector(5 downto 0);
        x_red   :out   std_logic_vector(6 downto 0);
        y_red   :out   std_logic_vector(7 downto 0);
        x_blue  :out   std_logic_vector(6 downto 0);
        y_blue  :out   std_logic_vector(7 downto 0)
);
end component;

signal quadrant: std_logic_vector(1 downto 0);
signal step: std_logic_vector(3 downto 0);

begin

label_controller: controller port map (
	clk => clk,
	reset => reset,
	left => left,
	right => right,
	quadrant => quadrant,
	step => step
);

label_coordinates: coordcalc port map (
	clk => clk,
	reset => reset,
	quadrant => quadrant,
	step => step,
	x_red => x_red,
	y_red => y_red,
	x_blue => x_blue,
	y_blue => y_blue
);

end behaviour;


