library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

architecture behaviour of hit_math_unit is

type calc_state is (reset_state, x0_comp, x1_comp, y0_comp, y1_comp, done_state);
signal current_state, next_state :calc_state := reset_state;
signal y_bottom :std_logic_vector(7 downto 0) := "00000000";
signal current_checks, next_checks :std_logic_vector(3 downto 0) := "0000";
signal current_enable, next_enable :std_logic;
signal do_rip :std_logic;

begin

rip <= current_enable;

P1: process(clk, reset, rip_reset)
begin

if (clk'event and clk = '1') then
	if (reset = '1') then
		current_state <= reset_state;
		next_enable <= '1';
	else
		if (rip_reset = '1' and current_enable = '1') then
			next_enable <= '0';
		elsif (do_rip = '1') then
			next_enable <= '1';
		end if;
		current_state <= next_state;
		current_checks <= next_checks;
		y_bottom <= std_logic_vector(unsigned(y_block) + unsigned(height));
		current_enable <= next_enable;
	end if;
end if;
end process;


P2: process(result, current_state, current_checks)
begin
	if (result = "00000000") then
	else
		case current_state is
			when x0_comp =>
				-- Left side of block should be left of circle (smaller x)
					if (result(7) = '0') then
					next_checks(0) <= '1';
				else
					next_checks(0) <= '0';
				end if;
				next_checks(1) <= current_checks(1);
				next_checks(2) <= current_checks(2);
				next_checks(3) <= current_checks(3);
			when x1_comp =>
				-- Right side of block should be right of circle (bigger x)
				if (result(7) = '1') then
					next_checks(1) <= '1';
				else
					next_checks(1) <= '0';
				end if;
				next_checks(0) <= current_checks(0);
				next_checks(2) <= current_checks(2);
				next_checks(3) <= current_checks(3);
			when y0_comp =>
				-- Top of block should be higher than the circle (smaller y)
				if (result(7) = '0') then
					next_checks(2) <= '1';
				else
					next_checks(2) <= '0';
				end if;
				next_checks(0) <= current_checks(0);
				next_checks(1) <= current_checks(1);
				next_checks(3) <= current_checks(3);
			when y1_comp =>
				--Bottom of block should be lower than the circle (smaller y)
				if (result(7) = '1') then
					next_checks(3) <= '1';
				else
					next_checks(3) <= '0';
				end if;
				next_checks(0) <= current_checks(0);
				next_checks(1) <= current_checks(1);
				next_checks(2) <= current_checks(2);
			when others =>
				next_checks <= "0000";
		end case;
	end if;
end process;



P3: process(current_state, start, current_checks, next_enable, x_circle, y_circle, x_block_0, x_block_1, y_block, y_bottom, current_enable)
begin
	case current_state is
		when x0_comp =>
			value1 <= "0" & x_circle;
			value2 <= "0" & x_block_0;
			next_state <= x1_comp;
			done <= '0';
			started <= '1';
			do_rip <= '0';

		when x1_comp =>
			value1 <= "0" & x_circle;
			value2 <= "0" & x_block_1;
			next_state <= y0_comp;
			done <= '0';
			started <= '1';
			do_rip <= '0';

		when y0_comp =>
			value1 <= y_circle;
			value2 <= y_block;
			next_state <= y1_comp;
			done <= '0';
			started <= '1';
			do_rip <= '0';

		when y1_comp =>
			value1 <= y_circle;
			value2 <= y_bottom;
			next_state <= done_state;
			done <= '0';
			started <= '1';
			do_rip <= '0';

		when done_state =>
			done <= '1';
			started <= '0';
			value1 <= "00000000";
			value2 <= "00000000";
			if (current_checks(0) = '1' and current_checks(1) = '1' and current_checks(2) = '1' and current_checks(3) = '1') then
				do_rip <= '1';
			else
				do_rip <= '0';
			end if;
			next_state <= reset_state;

		when reset_state =>
			done <= '0';
			value1 <= "00000000";
			value2 <= "00000000";
			do_rip <= '0';
			if (start = '1') then
				next_state <= x0_comp;
				started <= '1';
			else
				next_state <= current_state;
				started <= '0';
			end if;
	end case;
end process;

end behaviour;
