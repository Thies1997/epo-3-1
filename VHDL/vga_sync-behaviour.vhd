library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

architecture behaviour of vga_sync is
 	signal vcount : std_logic_vector(9 downto 0);
	signal hcount : std_logic_vector(7 downto 0);
	signal row : signal (8 downto 0);
	signal column : signal(7 downto 0);
begin


hcounter: process (clk, reset)
begin
	if reset='1' then 
		hcount <= (others => '0');
	elsif (clk'event and clk='1') then 
		if hcount=199 then 
			hcount <= (others => '0');
		else hcount <= hcount + 1;
		end if;
	end if;
end process;


process (hcount)
begin
	column <= hcount;
	if hcount>159 then 
		column <= (others => '0');
 	end if;
end process;


vcounter: process (clk, reset)
	begin
	if reset='1' then 
		vcount <= (others => '0');
	else if (clk'event and clk='1') then 
		if hcount=174 then 
			if vcount=524 then 
				vcount <= (others => '0');
			else vcount <= vcount + 1;
			end if;
		end if;
	end if;
	end if;
end process;


process (vcount)
begin
	row <= vcount(8 downto 0);
	if vcount>479 then 
		row <= (others => '0');
	end if;
end process;


sync: process (clk, reset)
begin
	if reset='1' then 
		hsync <= '0';
		vsync <= '0';
	elsif (clk'event and clk='1') then 
		if (hcount<=189 and hcount>=164) then 
			hsync <= '0';
		else hsync <= '1';
		end if;
		if (vcount<=494 and vcount>=493) then 
			vsync <= '0';
		else vsync <= '1';
		end if;
	end if;
end process;
row_address <= row(8 downto 2);
col_address <= column;

end behaviour;

