library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of typebuilder is

signal count		   :std_logic_vector (5 downto 0);
signal linear_feedback :std_logic;

begin

linear_feedback <= not(count(5) xor count(0));
blocktype <= (count(5) & count(3) & count(2));

P1: process (clk, reset)
begin
	if (reset = '1') then
		count <= (others=>'0');
	elsif (rising_edge(clk)) then
		count <= (count(3) & count(4) & count(2) & count(1) & count(0) & linear_feedback);
	end if;
end process;

end behaviour;
