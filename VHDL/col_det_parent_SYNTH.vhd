
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of col_det_parent is

   component subtraction
      port( clk, reset : in std_logic;  value1, value2 : in std_logic_vector (7
            downto 0);  result : out std_logic_vector (7 downto 0));
   end component;
   
   component hit_math_unit
      port( clk, reset, start : in std_logic;  x_circle : in std_logic_vector 
            (6 downto 0);  y_circle : in std_logic_vector (7 downto 0);  
            x_block_0, x_block_1 : in std_logic_vector (6 downto 0);  y_block :
            in std_logic_vector (7 downto 0);  height : in std_logic_vector (5 
            downto 0);  value1, value2 : out std_logic_vector (7 downto 0);  
            result : in std_logic_vector (7 downto 0);  started, done, rip : 
            out std_logic);
   end component;
   
   component hitman2
      port( clk, reset : in std_logic;  x_red, x_blue : in std_logic_vector (8 
            downto 0);  y_red, y_blue : in std_logic_vector (7 downto 0);  x00,
            x01, x10, x11 : in std_logic_vector (8 downto 0);  height : in 
            std_logic_vector (5 downto 0);  y0, y1 : in std_logic_vector (7 
            downto 0);  done, started : in std_logic;  start : out std_logic;  
            x_circle, x_block_0, x_block_1 : out std_logic_vector (6 downto 0);
            y_circle, y_block : out std_logic_vector (7 downto 0));
   end component;
   
   signal done, started, start, x_circle_6_port, x_circle_5_port, 
      x_circle_4_port, x_circle_3_port, x_circle_2_port, x_circle_1_port, 
      x_circle_0_port, x_block_0_6_port, x_block_0_5_port, x_block_0_4_port, 
      x_block_0_3_port, x_block_0_2_port, x_block_0_1_port, x_block_0_0_port, 
      x_block_1_6_port, x_block_1_5_port, x_block_1_4_port, x_block_1_3_port, 
      x_block_1_2_port, x_block_1_1_port, x_block_1_0_port, y_circle_7_port, 
      y_circle_6_port, y_circle_5_port, y_circle_4_port, y_circle_3_port, 
      y_circle_2_port, y_circle_1_port, y_circle_0_port, y_block_7_port, 
      y_block_6_port, y_block_5_port, y_block_4_port, y_block_3_port, 
      y_block_2_port, y_block_1_port, y_block_0_port, value1_7_port, 
      value1_6_port, value1_5_port, value1_4_port, value1_3_port, value1_2_port
      , value1_1_port, value1_0_port, value2_7_port, value2_6_port, 
      value2_5_port, value2_4_port, value2_3_port, value2_2_port, value2_1_port
      , value2_0_port, result_7_port, result_6_port, result_5_port, 
      result_4_port, result_3_port, result_2_port, result_1_port, result_0_port
      : std_logic;

begin
   
   hit_states : hitman2 port map( clk => clk, reset => reset, x_red(8) => 
                           x_red(8), x_red(7) => x_red(7), x_red(6) => x_red(6)
                           , x_red(5) => x_red(5), x_red(4) => x_red(4), 
                           x_red(3) => x_red(3), x_red(2) => x_red(2), x_red(1)
                           => x_red(1), x_red(0) => x_red(0), x_blue(8) => 
                           x_blue(8), x_blue(7) => x_blue(7), x_blue(6) => 
                           x_blue(6), x_blue(5) => x_blue(5), x_blue(4) => 
                           x_blue(4), x_blue(3) => x_blue(3), x_blue(2) => 
                           x_blue(2), x_blue(1) => x_blue(1), x_blue(0) => 
                           x_blue(0), y_red(7) => y_red(7), y_red(6) => 
                           y_red(6), y_red(5) => y_red(5), y_red(4) => y_red(4)
                           , y_red(3) => y_red(3), y_red(2) => y_red(2), 
                           y_red(1) => y_red(1), y_red(0) => y_red(0), 
                           y_blue(7) => y_blue(7), y_blue(6) => y_blue(6), 
                           y_blue(5) => y_blue(5), y_blue(4) => y_blue(4), 
                           y_blue(3) => y_blue(3), y_blue(2) => y_blue(2), 
                           y_blue(1) => y_blue(1), y_blue(0) => y_blue(0), 
                           x00(8) => x00(8), x00(7) => x00(7), x00(6) => x00(6)
                           , x00(5) => x00(5), x00(4) => x00(4), x00(3) => 
                           x00(3), x00(2) => x00(2), x00(1) => x00(1), x00(0) 
                           => x00(0), x01(8) => x01(8), x01(7) => x01(7), 
                           x01(6) => x01(6), x01(5) => x01(5), x01(4) => x01(4)
                           , x01(3) => x01(3), x01(2) => x01(2), x01(1) => 
                           x01(1), x01(0) => x01(0), x10(8) => x10(8), x10(7) 
                           => x10(7), x10(6) => x10(6), x10(5) => x10(5), 
                           x10(4) => x10(4), x10(3) => x10(3), x10(2) => x10(2)
                           , x10(1) => x10(1), x10(0) => x10(0), x11(8) => 
                           x11(8), x11(7) => x11(7), x11(6) => x11(6), x11(5) 
                           => x11(5), x11(4) => x11(4), x11(3) => x11(3), 
                           x11(2) => x11(2), x11(1) => x11(1), x11(0) => x11(0)
                           , height(5) => height(5), height(4) => height(4), 
                           height(3) => height(3), height(2) => height(2), 
                           height(1) => height(1), height(0) => height(0), 
                           y0(7) => y0(7), y0(6) => y0(6), y0(5) => y0(5), 
                           y0(4) => y0(4), y0(3) => y0(3), y0(2) => y0(2), 
                           y0(1) => y0(1), y0(0) => y0(0), y1(7) => y1(7), 
                           y1(6) => y1(6), y1(5) => y1(5), y1(4) => y1(4), 
                           y1(3) => y1(3), y1(2) => y1(2), y1(1) => y1(1), 
                           y1(0) => y1(0), done => done, started => started, 
                           start => start, x_circle(6) => x_circle_6_port, 
                           x_circle(5) => x_circle_5_port, x_circle(4) => 
                           x_circle_4_port, x_circle(3) => x_circle_3_port, 
                           x_circle(2) => x_circle_2_port, x_circle(1) => 
                           x_circle_1_port, x_circle(0) => x_circle_0_port, 
                           x_block_0(6) => x_block_0_6_port, x_block_0(5) => 
                           x_block_0_5_port, x_block_0(4) => x_block_0_4_port, 
                           x_block_0(3) => x_block_0_3_port, x_block_0(2) => 
                           x_block_0_2_port, x_block_0(1) => x_block_0_1_port, 
                           x_block_0(0) => x_block_0_0_port, x_block_1(6) => 
                           x_block_1_6_port, x_block_1(5) => x_block_1_5_port, 
                           x_block_1(4) => x_block_1_4_port, x_block_1(3) => 
                           x_block_1_3_port, x_block_1(2) => x_block_1_2_port, 
                           x_block_1(1) => x_block_1_1_port, x_block_1(0) => 
                           x_block_1_0_port, y_circle(7) => y_circle_7_port, 
                           y_circle(6) => y_circle_6_port, y_circle(5) => 
                           y_circle_5_port, y_circle(4) => y_circle_4_port, 
                           y_circle(3) => y_circle_3_port, y_circle(2) => 
                           y_circle_2_port, y_circle(1) => y_circle_1_port, 
                           y_circle(0) => y_circle_0_port, y_block(7) => 
                           y_block_7_port, y_block(6) => y_block_6_port, 
                           y_block(5) => y_block_5_port, y_block(4) => 
                           y_block_4_port, y_block(3) => y_block_3_port, 
                           y_block(2) => y_block_2_port, y_block(1) => 
                           y_block_1_port, y_block(0) => y_block_0_port);
   math_unit : hit_math_unit port map( clk => clk, reset => reset, start => 
                           start, x_circle(6) => x_circle_6_port, x_circle(5) 
                           => x_circle_5_port, x_circle(4) => x_circle_4_port, 
                           x_circle(3) => x_circle_3_port, x_circle(2) => 
                           x_circle_2_port, x_circle(1) => x_circle_1_port, 
                           x_circle(0) => x_circle_0_port, y_circle(7) => 
                           y_circle_7_port, y_circle(6) => y_circle_6_port, 
                           y_circle(5) => y_circle_5_port, y_circle(4) => 
                           y_circle_4_port, y_circle(3) => y_circle_3_port, 
                           y_circle(2) => y_circle_2_port, y_circle(1) => 
                           y_circle_1_port, y_circle(0) => y_circle_0_port, 
                           x_block_0(6) => x_block_0_6_port, x_block_0(5) => 
                           x_block_0_5_port, x_block_0(4) => x_block_0_4_port, 
                           x_block_0(3) => x_block_0_3_port, x_block_0(2) => 
                           x_block_0_2_port, x_block_0(1) => x_block_0_1_port, 
                           x_block_0(0) => x_block_0_0_port, x_block_1(6) => 
                           x_block_1_6_port, x_block_1(5) => x_block_1_5_port, 
                           x_block_1(4) => x_block_1_4_port, x_block_1(3) => 
                           x_block_1_3_port, x_block_1(2) => x_block_1_2_port, 
                           x_block_1(1) => x_block_1_1_port, x_block_1(0) => 
                           x_block_1_0_port, y_block(7) => y_block_7_port, 
                           y_block(6) => y_block_6_port, y_block(5) => 
                           y_block_5_port, y_block(4) => y_block_4_port, 
                           y_block(3) => y_block_3_port, y_block(2) => 
                           y_block_2_port, y_block(1) => y_block_1_port, 
                           y_block(0) => y_block_0_port, height(5) => height(5)
                           , height(4) => height(4), height(3) => height(3), 
                           height(2) => height(2), height(1) => height(1), 
                           height(0) => height(0), value1(7) => value1_7_port, 
                           value1(6) => value1_6_port, value1(5) => 
                           value1_5_port, value1(4) => value1_4_port, value1(3)
                           => value1_3_port, value1(2) => value1_2_port, 
                           value1(1) => value1_1_port, value1(0) => 
                           value1_0_port, value2(7) => value2_7_port, value2(6)
                           => value2_6_port, value2(5) => value2_5_port, 
                           value2(4) => value2_4_port, value2(3) => 
                           value2_3_port, value2(2) => value2_2_port, value2(1)
                           => value2_1_port, value2(0) => value2_0_port, 
                           result(7) => result_7_port, result(6) => 
                           result_6_port, result(5) => result_5_port, result(4)
                           => result_4_port, result(3) => result_3_port, 
                           result(2) => result_2_port, result(1) => 
                           result_1_port, result(0) => result_0_port, started 
                           => started, done => done, rip => rip);
   subtractor : subtraction port map( clk => clk, reset => reset, value1(7) => 
                           value1_7_port, value1(6) => value1_6_port, value1(5)
                           => value1_5_port, value1(4) => value1_4_port, 
                           value1(3) => value1_3_port, value1(2) => 
                           value1_2_port, value1(1) => value1_1_port, value1(0)
                           => value1_0_port, value2(7) => value2_7_port, 
                           value2(6) => value2_6_port, value2(5) => 
                           value2_5_port, value2(4) => value2_4_port, value2(3)
                           => value2_3_port, value2(2) => value2_2_port, 
                           value2(1) => value2_1_port, value2(0) => 
                           value2_0_port, result(7) => result_7_port, result(6)
                           => result_6_port, result(5) => result_5_port, 
                           result(4) => result_4_port, result(3) => 
                           result_3_port, result(2) => result_2_port, result(1)
                           => result_1_port, result(0) => result_0_port);

end synthesised;



