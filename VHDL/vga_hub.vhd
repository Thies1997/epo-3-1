library IEEE;
use IEEE.std_logic_1164.ALL;

entity vga_hub is
	PORT(clk, reset, enable 	:in 	std_logic;
    	x_b1     	:in	std_logic_vector(6 downto 0);
    	y_b1     	:in	std_logic_vector(7 downto 0);
    	x_b2     	:in	std_logic_vector(6 downto 0);
    	y_b2     	:in	std_logic_vector(7 downto 0);
    	y_o1     	:in	std_logic_vector(7 downto 0);
    	t_o1     	:in	std_logic_vector(2 downto 0);
		y_o2     	:in	std_logic_vector(7 downto 0);
    	t_o2     	:in	std_logic_vector(2 downto 0);
		y_o3     	:in	std_logic_vector(7 downto 0);
    	t_o3     	:in	std_logic_vector(2 downto 0);
		y_o4     	:in	std_logic_vector(7 downto 0);
    	t_o4     	:in	std_logic_vector(2 downto 0);
		row 	  :in 	std_logic_vector(8 downto 0);
        column 	  :in 	std_logic_vector(7 downto 0);
		vga_red, vga_green, vga_blue : out std_logic
	);
end vga_hub;

