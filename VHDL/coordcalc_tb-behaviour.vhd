library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of coordcalc_tb is
component coordcalc is 
	port(clk:in    std_logic;
	reset	:in    std_logic;
	quadrant:in    std_logic_vector(1 downto 0);
        step    :in    std_logic_vector(3 downto 0);
        x_red   :out   std_logic_vector(8 downto 0);
        y_red   :out   std_logic_vector(7 downto 0);
        x_blue  :out   std_logic_vector(8 downto 0);
        y_blue  :out   std_logic_vector(7 downto 0));
end component;
signal clk, reset: std_logic;
signal quadrant: std_logic_vector(1 downto 0);
signal step: std_logic_vector(3 downto 0);
signal x_red, x_blue: std_logic_vector(8 downto 0);
signal y_red, y_blue: std_logic_vector(7 downto 0);
begin
	lbl1: coordcalc port map(clk, reset, quadrant, step, x_red, y_red, x_blue, y_blue);
		clk <= '1' after 0 ns, '0' after 1 ns when clk /= '0' else '1' after 1 ns;
		reset <= '1', '0' after 3 ns;
		quadrant <= "00" after 0 ns, "01" after 100 ns, "11" after 200 ns, "10" after 300 ns;
		step <= "0000", "0001" after 10 ns, "0010" after 20 ns, "0011" after 30 ns, "0100" after 40 ns, "0101" after 50 ns, "0110" after 60 ns, "0111" after 70 ns,
			"0000" after 100 ns, "0001" after 110 ns, "0010" after 120 ns, "0011" after 130 ns, "0100" after 140 ns, "0101" after 150 ns, "0110" after 160 ns, "0111" after 170 ns,
			"0000" after 200 ns, "0001" after 210 ns, "0010" after 220 ns, "0011" after 230 ns, "0100" after 240 ns, "0101" after 250 ns, "0110" after 260 ns, "0111" after 270 ns,
			"0000" after 300 ns, "0001" after 310 ns, "0010" after 320 ns, "0011" after 330 ns, "0100" after 340 ns, "0101" after 350 ns, "0110" after 360 ns, "0111" after 370 ns;

end behaviour;











