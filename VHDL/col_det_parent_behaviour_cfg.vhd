configuration col_det_parent_behaviour_cfg of col_det_parent is
   for behaviour
      for all: hitman2 use configuration work.hitman2_behaviour_cfg;
      end for;
      for all: hit_math_unit use configuration work.hit_math_unit_behaviour_cfg;
      end for;
      for all: subtraction use configuration work.subtraction_behaviour_cfg;
      end for;
   end for;
end col_det_parent_behaviour_cfg;


