
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of subtraction is

   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component dfr11
      port( D, R, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal N2, N3, N4, N5, N6, N7, N8, N9, n2_port, n3_port, sub_14_n47, 
      sub_14_n46, sub_14_n45, sub_14_n44, sub_14_n43, sub_14_n42, sub_14_n41, 
      sub_14_n40, sub_14_n39, sub_14_n38, sub_14_n37, sub_14_n36, sub_14_n35, 
      sub_14_n34, sub_14_n33, sub_14_n32, sub_14_n31, sub_14_n30, sub_14_n29, 
      sub_14_n28, sub_14_n27, sub_14_n26, sub_14_n25, sub_14_n24, sub_14_n23, 
      sub_14_n22, sub_14_n21, sub_14_n20, sub_14_n19, sub_14_n18, sub_14_n17, 
      sub_14_n16, sub_14_n15, sub_14_n14, sub_14_n13, sub_14_n12, sub_14_n11, 
      sub_14_n10, sub_14_n9, sub_14_n7, sub_14_n6, sub_14_n5, sub_14_n4, 
      sub_14_n3, sub_14_n2, sub_14_n1 : std_logic;

begin
   
   result_reg_7_inst : dfr11 port map( D => N9, R => reset, CK => n3_port, Q =>
                           result(7));
   result_reg_6_inst : dfr11 port map( D => N8, R => reset, CK => n3_port, Q =>
                           result(6));
   result_reg_5_inst : dfr11 port map( D => N7, R => reset, CK => n3_port, Q =>
                           result(5));
   result_reg_4_inst : dfr11 port map( D => N6, R => reset, CK => n3_port, Q =>
                           result(4));
   result_reg_3_inst : dfr11 port map( D => N5, R => reset, CK => n3_port, Q =>
                           result(3));
   result_reg_2_inst : dfr11 port map( D => N4, R => reset, CK => n3_port, Q =>
                           result(2));
   result_reg_1_inst : dfr11 port map( D => N3, R => reset, CK => n3_port, Q =>
                           result(1));
   result_reg_0_inst : dfr11 port map( D => N2, R => reset, CK => n3_port, Q =>
                           result(0));
   n2_port <= '0';
   U5 : iv110 port map( A => clk, Y => n3_port);
   sub_14_U54 : no210 port map( A => sub_14_n13, B => value1(0), Y => 
                           sub_14_n45);
   sub_14_U53 : na210 port map( A => value1(0), B => sub_14_n13, Y => 
                           sub_14_n47);
   sub_14_U52 : na210 port map( A => sub_14_n12, B => sub_14_n47, Y => N2);
   sub_14_U51 : ex210 port map( A => value2(1), B => value1(1), Y => sub_14_n46
                           );
   sub_14_U50 : ex210 port map( A => sub_14_n45, B => sub_14_n46, Y => N3);
   sub_14_U49 : na210 port map( A => value1(1), B => sub_14_n12, Y => 
                           sub_14_n42);
   sub_14_U48 : no210 port map( A => sub_14_n12, B => value1(1), Y => 
                           sub_14_n44);
   sub_14_U47 : no210 port map( A => sub_14_n44, B => value2(1), Y => 
                           sub_14_n43);
   sub_14_U46 : na210 port map( A => sub_14_n42, B => sub_14_n11, Y => 
                           sub_14_n40);
   sub_14_U45 : ex210 port map( A => value1(2), B => value2(2), Y => sub_14_n41
                           );
   sub_14_U44 : ex210 port map( A => sub_14_n10, B => sub_14_n41, Y => N4);
   sub_14_U43 : na210 port map( A => value1(2), B => sub_14_n40, Y => 
                           sub_14_n37);
   sub_14_U42 : no210 port map( A => sub_14_n40, B => value1(2), Y => 
                           sub_14_n39);
   sub_14_U41 : no210 port map( A => sub_14_n39, B => value2(2), Y => 
                           sub_14_n38);
   sub_14_U40 : na210 port map( A => sub_14_n37, B => sub_14_n9, Y => 
                           sub_14_n33);
   sub_14_U39 : ex210 port map( A => value1(3), B => value2(3), Y => sub_14_n36
                           );
   sub_14_U38 : ex210 port map( A => sub_14_n33, B => sub_14_n36, Y => 
                           sub_14_n35);
   sub_14_U37 : na210 port map( A => value1(3), B => sub_14_n33, Y => 
                           sub_14_n34);
   sub_14_U36 : no210 port map( A => sub_14_n33, B => value1(3), Y => 
                           sub_14_n32);
   sub_14_U35 : no210 port map( A => sub_14_n32, B => value2(3), Y => 
                           sub_14_n31);
   sub_14_U34 : no210 port map( A => sub_14_n7, B => sub_14_n31, Y => 
                           sub_14_n29);
   sub_14_U33 : ex210 port map( A => value1(4), B => value2(4), Y => sub_14_n30
                           );
   sub_14_U32 : ex210 port map( A => sub_14_n29, B => sub_14_n30, Y => N6);
   sub_14_U31 : no210 port map( A => sub_14_n6, B => sub_14_n29, Y => 
                           sub_14_n26);
   sub_14_U30 : na210 port map( A => sub_14_n29, B => sub_14_n6, Y => 
                           sub_14_n28);
   sub_14_U29 : no210 port map( A => sub_14_n5, B => value2(4), Y => sub_14_n27
                           );
   sub_14_U28 : no210 port map( A => sub_14_n26, B => sub_14_n27, Y => 
                           sub_14_n24);
   sub_14_U27 : ex210 port map( A => value1(5), B => value2(5), Y => sub_14_n25
                           );
   sub_14_U26 : ex210 port map( A => sub_14_n24, B => sub_14_n25, Y => N7);
   sub_14_U25 : na210 port map( A => value1(5), B => sub_14_n4, Y => sub_14_n23
                           );
   sub_14_U24 : no210 port map( A => sub_14_n4, B => value1(5), Y => sub_14_n22
                           );
   sub_14_U23 : no210 port map( A => sub_14_n22, B => value2(5), Y => 
                           sub_14_n21);
   sub_14_U22 : no210 port map( A => sub_14_n3, B => sub_14_n21, Y => 
                           sub_14_n18);
   sub_14_U21 : ex210 port map( A => value1(6), B => value2(6), Y => sub_14_n20
                           );
   sub_14_U20 : ex210 port map( A => sub_14_n18, B => sub_14_n20, Y => N8);
   sub_14_U19 : no210 port map( A => value1(6), B => sub_14_n2, Y => sub_14_n19
                           );
   sub_14_U18 : no210 port map( A => value2(6), B => sub_14_n19, Y => 
                           sub_14_n16);
   sub_14_U17 : no210 port map( A => sub_14_n18, B => sub_14_n1, Y => 
                           sub_14_n17);
   sub_14_U16 : no210 port map( A => sub_14_n16, B => sub_14_n17, Y => 
                           sub_14_n14);
   sub_14_U15 : ex210 port map( A => value2(7), B => value1(7), Y => sub_14_n15
                           );
   sub_14_U14 : ex210 port map( A => sub_14_n14, B => sub_14_n15, Y => N9);
   sub_14_U13 : iv110 port map( A => value2(0), Y => sub_14_n13);
   sub_14_U12 : iv110 port map( A => sub_14_n45, Y => sub_14_n12);
   sub_14_U11 : iv110 port map( A => sub_14_n43, Y => sub_14_n11);
   sub_14_U10 : iv110 port map( A => sub_14_n40, Y => sub_14_n10);
   sub_14_U9 : iv110 port map( A => sub_14_n38, Y => sub_14_n9);
   sub_14_U8 : iv110 port map( A => sub_14_n35, Y => N5);
   sub_14_U7 : iv110 port map( A => sub_14_n34, Y => sub_14_n7);
   sub_14_U6 : iv110 port map( A => value1(4), Y => sub_14_n6);
   sub_14_U5 : iv110 port map( A => sub_14_n28, Y => sub_14_n5);
   sub_14_U4 : iv110 port map( A => sub_14_n24, Y => sub_14_n4);
   sub_14_U3 : iv110 port map( A => sub_14_n23, Y => sub_14_n3);
   sub_14_U2 : iv110 port map( A => sub_14_n18, Y => sub_14_n2);
   sub_14_U1 : iv110 port map( A => value1(6), Y => sub_14_n1);

end synthesised;



