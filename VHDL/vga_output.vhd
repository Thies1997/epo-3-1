library IEEE;
use IEEE.std_logic_1164.ALL;

entity vga_output is
	port(enable,rom_mux_output,rom2_mux_output:in    std_logic;
		color	:in std_logic_vector(1 downto 0);
        vga_red       :out   std_logic;
        vga_green     :out   std_logic;
        vga_blue      :out   std_logic);
end vga_output;
