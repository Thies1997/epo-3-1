library IEEE;
use IEEE.std_logic_1164.ALL;


architecture behaviour of bar_control_tb is
  

 component bar_control is
	port( clk          :in     std_logic;
	      enable       :in     std_logic;
		    reset        :in     std_logic;
        output_y0    :out    unsigned(7 downto 0);
        output_y1    :out    unsigned(7 downto 0);
        output_y2    :out    unsigned(7 downto 0);
        output_y3    :out    unsigned(7 downto 0);
        output_type0 :out    std_logic_vector(2 downto 0);
        output_type1 :out    std_logic_vector(2 downto 0);
        output_type2 :out    std_logic_vector(2 downto 0);
        output_type3 :out    std_logic_vector(2 downto 0)
	);
end component;

signal clk, enable, reset: std_logic; 
signal output_y0, output_y1, output_y2, output_y3: unsigned (7 downto 0);
signal output_type0, output_type1, output_type2, output_type3: std_logic_vector (2 downto 0);

begin
  P1: 
    bar_control port map (
      clk, 
      enable,
      reset, 
      output_y0,
      output_y1,
      output_y2, 
      output_y3,  
      output_type0,
      output_type1,
      output_type2,
      output_type3
    );
    
    
    clk <= '1' after 0 ns, '0' after 81.78 ns when clk /= '0' else '1' after 81.78 ns;              
    reset <= '1' after 0 ns, '0' after 20 ms;
    enable <= '1' after 0 ns;
     
end behaviour;

